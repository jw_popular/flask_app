import requests
import urllib3

urllib3.disable_warnings()

# url = "https://192.168.10.228/jaws/config/outlets"
url_all = "https://192.168.10.228/jaws/control/outlets"
url_list = ["https://192.168.10.228/jaws/control/outlets/AA1",
            "https://192.168.10.228/jaws/control/outlets/AA2",
            "https://192.168.10.228/jaws/control/outlets/AA3",
            "https://192.168.10.228/jaws/control/outlets/AA4",
            "https://192.168.10.228/jaws/control/outlets/AA5",
            "https://192.168.10.228/jaws/control/outlets/AA6",
            "https://192.168.10.228/jaws/control/outlets/AA7",
            "https://192.168.10.228/jaws/control/outlets/AA8",
            "https://192.168.10.228/jaws/control/outlets/AA9",
            "https://192.168.10.228/jaws/control/outlets/AA10",
            "https://192.168.10.228/jaws/control/outlets/AA11",
            "https://192.168.10.228/jaws/control/outlets/AA12",
            "https://192.168.10.228/jaws/control/outlets/AA13",
            "https://192.168.10.228/jaws/control/outlets/AA14",
            "https://192.168.10.228/jaws/control/outlets/AA15",
            "https://192.168.10.228/jaws/control/outlets/AA16"]

# resp = requests.get(url)
# resp = requests.get(url, headers={'Accept': 'application/json'}, verify=False)
get_resp = requests.get(url_all, auth=('admn', '1234'), headers={'Accept': 'application/json'}, verify=False)

print(get_resp.status_code)
print(get_resp.headers["Content-Type"])

print(get_resp.json())

todo = {"control_action": "on"}

for url in url_list:
    set_resp = requests.patch(url, auth=('admn', '1234'), json=todo, verify=False)
    print(set_resp.status_code)
