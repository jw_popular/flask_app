import paramiko

try:
    client = paramiko.SSHClient()
    client.set_missing_host_key_policy(paramiko.AutoAddPolicy())
    client.connect(hostname="192.168.10.228", port=22, username="admn", password="1234", disabled_algorithms=dict(pubkeys=["rsa-sha2-256"]))
    # t = client.get_transport()
    # sftp=paramiko.SFTPClient.from_transport(t)
    # d = sftp.stat("/Users/allen/Dropbox/python/ssh.txt")
    # print (d)
    stdin, stdout, stderr = client.exec_command('status')
    # stdin, stdout, stderr = client.exec_command('ls -al')
    result = stdout.readlines()
    print (result)
except Exception:
    print ('Exception!!')
    raise